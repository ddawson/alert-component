({
    getAllAlerts : function(component) {
        var action = component.get("c.getAllAlerts");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.allAlertWrappers", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getAllAlertProfiles: function(component) {
        var errorAction = component.get("c.retrieveAlertProfilingHandler");
        var lightningProcess = component.get("v.processName");
        
        errorAction.setParams({
            "lightningProcess" : lightningProcess
        });
        
        errorAction.setCallback(this, function(response){
            var errorState = response.getState();
            if (component.isValid() && errorState === "SUCCESS") {
                component.set("v.alertProfiles", response.getReturnValue());
            }
        });
        
        $A.enqueueAction(errorAction);
    },
    addAlertAction : function(component, event){
        var alertId = event.getParam("alertId");
        var allAlertWrappers = component.get("v.allAlertWrappers");
        var alertProfiles = component.get("v.alertProfiles");
        var key;
        var addedAlertIds = component.get("v.addedAlertIds");
        var successAlert;
        var infoAlert;
        var errorAlert;
        var warningAlert;
        for(key in allAlertWrappers){
            debugger;
            if(key == alertId && addedAlertIds.indexOf(alertId)== -1){
                addedAlertIds.push(alertId);
                var addAlert = allAlertWrappers[key];
                var errorCode = event.getParam("errorCode");
                var billingSystem = event.getParam("externalSystem");
                for(key in alertProfiles){
                    if(alertProfiles[key].ErrorCode__c == errorCode && alertProfiles[key].BillingSystem__c == billingSystem){
                        var profile = alertProfiles[key];
                        addAlert.alertType = profile.ErrorLevel__c;
                    }
                }
                if(addAlert.alertType == 'Success'){
                    successAlert = addAlert;
                }else if(addAlert.alertType == 'Warning'){
                    warningAlert = addAlert;
                }else if(addAlert.alertType == 'Information'){
                    infoAlert = addAlert;
                }else if(addAlert.alertType == 'Error'){
                    errorAlert = addAlert;
                }
            }
        }
        var mergeParamMap = event.getParam("mergeParamMap");
        for (key in mergeParamMap) {
            var value =  mergeParamMap[key];
            if(successAlert){
                successAlert.alertMessage = successAlert.alertMessage.replace(key, value);
                //ND 3/11/16: For NCS606
                if(profile){
                    if(profile.TranslationMethod__c == 'Replace'){
                        successAlert.alertMessage = successAlert.alertMessage.replace(profile.System_Error__c,profile.Translation_Message__c);
                    }else if(profile.TranslationMethod__c == 'Overwrite'){
                        successAlert.alertMessage = profile.Translation_Message__c;
                    }
                }
            }
            if(errorAlert){
                errorAlert.alertMessage = errorAlert.alertMessage.replace(key, value);
                //ND 3/11/16: For NCS606
                if(profile){
                    if(profile.TranslationMethod__c == 'Replace'){
                        errorAlert.alertMessage = errorAlert.alertMessage.replace(profile.System_Error__c,profile.Translation_Message__c);
                    }else if(profile.TranslationMethod__c == 'Overwrite'){
                        errorAlert.alertMessage = profile.Translation_Message__c;
                    }
                }
            }
            if(infoAlert){
                infoAlert.alertMessage = infoAlert.alertMessage.replace(key, value);
                //ND 3/11/16: For NCS606
                if(profile){
                    if(profile.TranslationMethod__c == 'Replace'){
                        infoAlert.alertMessage = infoAlert.alertMessage.replace(profile.System_Error__c,profile.Translation_Message__c);
                    }else if(profile.TranslationMethod__c == 'Overwrite'){
                        infoAlert.alertMessage = profile.Translation_Message__c;
                    }
                }
            }
            if(warningAlert){
                warningAlert.alertMessage = warningAlert.alertMessage.replace(key, value);
                //ND 3/11/16: For NCS606
                if(profile){
                    if(profile.TranslationMethod__c == 'Replace'){
                        warningAlert.alertMessage = warningAlert.alertMessage.replace(profile.System_Error__c,profile.Translation_Message__c);
                    }else if(profile.TranslationMethod__c == 'Overwrite'){
                        warningAlert.alertMessage = profile.Translation_Message__c;
                    }
                }
            }
        }
        if(successAlert){
            var existingSuccessAlerts = component.get("v.successAlerts");
            existingSuccessAlerts.push(successAlert);
            component.set("v.successAlerts", existingSuccessAlerts);
        }
        if(errorAlert){
            var existingErrorAlerts = component.get("v.errorAlerts");
            existingErrorAlerts.push(errorAlert);
            component.set("v.errorAlerts", existingErrorAlerts);
        }
        if(infoAlert){
            var existingInfoAlerts = component.get("v.infoAlerts");
            existingInfoAlerts.push(infoAlert);
            component.set("v.infoAlerts", existingInfoAlerts);
        }
        if(warningAlert){
            var existingWarningAlerts = component.get("v.warningAlerts");
            existingWarningAlerts.push(warningAlert);
            component.set("v.warningAlerts", existingWarningAlerts);
        }
    },
    removeAlert: function(component, event){
        var selectedItem = event.currentTarget;
        var removeAlertId = selectedItem.dataset.alertid;
        
        var index = selectedItem.dataset.index;
        var addedAlertIds = component.get("v.addedAlertIds");
        addedAlertIds.splice(addedAlertIds.indexOf(removeAlertId), 1);
        component.set("v.addedAlertIds", addedAlertIds);
        var alertBox = event.currentTarget.parentElement;
        $A.util.toggleClass(alertBox, 'slds-hide');
    },
    removeAlertAction: function(component, event){
        var removeAlertId = event.getParam("alertId");
        
        var index = selectedItem.dataset.index;
        var addedAlertIds = component.get("v.addedAlertIds");
        addedAlertIds.splice(addedAlertIds.indexOf(removeAlertId), 1);
        component.set("v.addedAlertIds", addedAlertIds);
        var alertBox = event.currentTarget.parentElement;
        $A.util.toggleClass(alertBox, 'slds-hide');
    },
    handleExternalError: function(component, event){
        
    },
    removeAllAlerts: function(component, event){
        // 2016-10-28 Item-00603 louis.wang@bluewolf.com - remove alerts so they are not carried over when switching subscription
        
        // clear the array for addedAlertIds
        var addedAlertIds = component.get("v.addedAlertIds");
        addedAlertIds.splice(0, addedAlertIds.length);
        component.set("v.addedAlertIds", addedAlertIds);
        
        // clear the array for successAlerts        	
        var successAlerts = component.get("v.successAlerts");
        successAlerts.splice(0, successAlerts.length);
        component.set("v.successAlerts", successAlerts);
        
        // clear the array for errorAlerts
        var errorAlerts = component.get("v.errorAlerts");
        errorAlerts.splice(0, errorAlerts.length);
        component.set("v.errorAlerts", errorAlerts);
        
        // clear the array for infoAlerts
        var infoAlerts = component.get("v.infoAlerts");
        infoAlerts.splice(0, infoAlerts.length);
        component.set("v.infoAlerts", infoAlerts);
        
        // clear the array for warningAlerts
        var warningAlerts = component.get("v.warningAlerts");
        warningAlerts.splice(0, warningAlerts.length);
        component.set("v.warningAlerts", warningAlerts);
    }
})