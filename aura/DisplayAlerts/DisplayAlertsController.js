({
	doInit : function(component, event, helper) {
		helper.getAllAlerts(component);
		helper.getAllAlertProfiles(component);
	},
    addAlertAction : function(component, event, helper) {
        helper.addAlertAction(component, event);
	},
    removeAlert : function(component, event, helper) {
        helper.removeAlert(component, event);
	},
    removeAlertAction : function(component, event, helper) {
        helper.removeAlertAction(component, event);
	},
    removeAllAlerts : function(component, event, helper){
        helper.removeAllAlerts(component, event);
    }

})