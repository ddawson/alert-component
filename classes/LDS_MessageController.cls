public class LDS_MessageController {
    static String SUCCESS = 'Success';
    static String WARN = 'Warning';
    static String INFO = 'Information';
    static String ERROR = 'Error';

    @AuraEnabled
    public static Message_Details__c retrieveMsgInfo(String msgId){
        return Message_Details__c.getAll().get(msgId);
    }

    @AuraEnabled
    public static Map<String, LDS_AlertWrapper> getAllAlerts(){
    	Map<String, LDS_AlertWrapper> alertWrappers = new Map<String, LDS_AlertWrapper>();
        for (Message_Details__c alertDetails: Message_Details__c.getAll().values()) {
            LDS_AlertWrapper alertWrapper = new LDS_AlertWrapper();
            alertWrapper.alertId = alertDetails.name;
            alertWrapper.alertType = alertDetails.Message_Type__c;
            alertWrapper.alertMessage = alertDetails.Message__c;
            alertWrappers.put(alertDetails.name, alertWrapper);
        }

        return alertWrappers;
    }

    /*@auraEnabled
    public static String accessAlertProfilingHandler(String billingSystem, String errorCode, String lightningProcess){
        return AlertProfilingHandlerClass.AlertProfilingHandler(billingSystem, errorCode, lightningProcess);
    }*/

    @auraEnabled
    public static Map<Id, AlertProfiling__c> retrieveAlertProfilingHandler(String lightningProcess){
        return AlertProfilingHandlerClass.retrieveAlertProfilingRecords(lightningProcess);
    }

}