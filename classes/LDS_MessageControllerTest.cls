@isTest
private class LDS_MessageControllerTest {
	@testSetup static void setup() {
		List<Message_Details__c> alerts = new List<Message_Details__c> {
			new Message_Details__c(Name = 'Alert1', Message_Type__c = 'Success', Message__c = 'Test message 1'),
			new Message_Details__c(Name = 'Alert2', Message_Type__c = 'Warning', Message__c = 'Test message 2'),
			new Message_Details__c(Name = 'Alert3', Message_Type__c = 'Information', Message__c = 'Test message 3'),
			new Message_Details__c(Name = 'Alert4', Message_Type__c = 'Error', Message__c = 'Test message 4')
		};
		insert alerts;
	}

    static testMethod void testRetrieveMsgInfo() {
    	Test.startTest();
    	
        Message_Details__c m = LDS_MessageController.retrieveMsgInfo('Alert1');
        
        Test.stopTest(); 
        
        System.assertEquals('Success', m.Message_Type__c);       
    }
    
    static testMethod void testGetAllAlerts() {
        Test.startTest();
        
        Map<String, LDS_AlertWrapper> allAlerts = LDS_MessageController.getAllAlerts();
        
        Test.stopTest();  
        
        System.assertEquals(4, allAlerts.size());      
    }
}