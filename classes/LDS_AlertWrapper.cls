public class LDS_AlertWrapper {
    @AuraEnabled public String alertId {set;get;}
    @AuraEnabled public String alertType {set;get;}
    @AuraEnabled public String alertMessage {set;get;}    
}