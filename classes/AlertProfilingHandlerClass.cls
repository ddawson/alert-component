public class AlertProfilingHandlerClass {

    public static Map<Id, AlertProfiling__c> retrieveAlertProfilingRecords(String lightningProcess){
        if(lightningProcess != null){
            //ND 3/11/16: For NCS606
            String lightningProcessCentre = '%'+lightningProcess+'%';
            String lightningProcessLeft = lightningProcess+'%';
            String lightningProcessRight = '%'+lightningProcess;
          Map<Id, AlertProfiling__c> apList = new Map<Id, AlertProfiling__c>([SELECT Id, ErrorLevel__c, BillingSystem__c, ErrorCode__c, LightningProcess__c, System_Error__c, Translation_Message__c, TranslationMethod__c FROM AlertProfiling__c WHERE LightningProcess__c like:lightningProcessCentre OR LightningProcess__c like:lightningProcessLeft OR LightningProcess__c like:lightningProcessRight]);
          return apList;
        }
        return null;
    }
}